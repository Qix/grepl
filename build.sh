cd ./src
src=`find ./ -type f`
cd ..
while read line; do
	echo -e "\x1B[30;1m->\x1B[0m $line"
	coffee -o ./lib/`dirname "$line"` -c "./src/$line"
done <<< "$src"