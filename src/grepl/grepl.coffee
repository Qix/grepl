#
# GREPL
#	A Greater REPL

# Get JANSI
jansi = require 'jansi'
keypress = require 'keypress'

# Store out
out = process.stdout
inp = process.openStdin()
keypress inp

# String insert utility
strins = (src, sub, ins = src.length) ->
	"#{src.substring 0, ins}#{sub}#{src.substring ins}"

# Command class
#	Validates a chunk of
#	code and determines when it is closed
class CommandParser
	constructor: () ->
		# Setup flags
		@braceDepth = 0
		@parenDepth = 0
		@string = null
		@command = ""

	# Whether or not the command
	#	can be executed and not
	#	fail to depth issues.
	isComplete: () ->
		@braceDepth is 0 and
		@parenDepth is 0 and
		@string is null

	# Adds a line of code
	add: (str) ->
		# Correct
		str = str.replace /[\r\n]*$/, '\n'

		# Parse
		@parse str

		# Append
		@command += str

	# Parses a line of code and
	#	adds counts/flags
	parse: (str) ->
		# Iterate characters
		for c in str
			# Switch character
			switch c
				when '{' then ++@braceDepth if @string is null
				when '}' then --@braceDepth if @string is null
				when '(' then ++@parenDepth if @string is null
				when ')' then --@parenDepth if @string is null
				when '"' or '\''
					if @string is null then @string = c
					else if @string is c then @string = null
					break

# Line buffer class
#	Keeps track of ANSI stuff!
class LineBuffer
	# @param callback Called when a newline is hit
	constructor: (@callback) ->
		# Setup buffers
		@buffer = ""
		@cursor = -1;

	# Clears the line and resets the prompt
	clearLine: () ->
		# Clear the line
		jansi.clearLine 'all'
		jansi.leftAbs 0

		# Prompt!
		out.write config.prompt

		# Get lengths and output placeholder
		#	We subtract 1 to give the cursor a
		#	block of space
		blen = @getBufferCapacity() - 1
		promptText = @buffer

		# Cut if necessary
		if blen < @buffer.length
			promptText = @buffer.substring @buffer.length - blen

		# Write
		out.write promptText

	# Calculates the number of characters we have
	#	available to display text
	getBufferCapacity: () ->
		# Get console width
		cWidth = out.columns

		# Subtract prompt length
		cWidth - (jansi.plain config.prompt).length

	# Add to buffer
	append: (char) ->
		@buffer = strins @buffer, char, (if @cursor < 0 then null else @cursor)
		if char is '\n' or char is '\r'
			out.write char
			@callback @buffer
			@buffer = ""
		++@cursor if @cursor >= 0
		@clearLine()

	reset: () ->
		@buffer = ""
		@clearLine()

	empty: () ->
		@buffer is ""

	backspace: () ->
		@buffer = @buffer.substring 0, @buffer.length - 1
		@clearLine()

	left: () ->
		jansi.left()
		@cursor = @buffer.length if @cursor < 0
		--@cursor if @cursor > 0

	right: () ->
		return if @cursor is @buffer.length or @cursor < 0
		jansi.right()
		@cursor = @buffer.length if @cursor < 0
		--@cursor

# Line handler
lineHandler = (line) ->
	# Add the line
	config['parser'].add line

	# Is it complete?
	if config['parser'].isComplete()
		command = config['parser'].command
		config['parser'] = new CommandParser()

		try
			result = (eval).call config.global, command
			console.log jansi.$ "\n$3#{result}$!"
		catch e
			console.error jansi.$ "$C#{e}$!"

# Configuration
config =
	prompt: jansi.$ "$8> $!"
	start: () ->
		# Set raw mode
		inp.setRawMode on
		inp.resume()
		inp.setEncoding 'utf8'
		config.buffer.clearLine()
	end: () ->
		# Set raw mode
		inp.setRawMode off
		inp.pause()
		console.log()
	onLine: lineHandler
	onSigint: () -> process.exit()
	buffer: new LineBuffer lineHandler
	global: global
	parser: new CommandParser()

# Force require
config.global['load'] = (file) -> require file

# Setup keypress handler
inp.on 'keypress', (chunk, key) ->
	switch
		when key and key.sequence is '\u0003'
			if config.buffer.empty() then config.onSigint()
			else config.buffer.reset()
			break
		when key and key.code is '[D' then config.buffer.left()
		when key and key.code is '[C' then config.buffer.right()
		when key and key.sequence is '\b' then config.buffer.backspace()
		else config.buffer.append chunk if chunk?

# Export
module.exports = config